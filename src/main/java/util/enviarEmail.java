package util;

import org.apache.commons.mail.*;

public class enviarEmail {

    public void enviar(String caminho, String nome_arquivo){
        String seuEmail = "parentejohnny9@gmail.com";
        String senha = "";
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(seuEmail, senha));
        email.setSSLOnConnect(true);

        try {
            email.setFrom(seuEmail);
            email.setSubject("Planilhas de vendas validadas!");
            email.setMsg("Olá, tudo bem? segue a baixo o anexo das planilhas que foram validadas pelo sistema.");
            email.addTo(seuEmail);
            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath(caminho);
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setName(nome_arquivo);
            email.attach(attachment);
            email.send();
            System.out.println(nome_arquivo);
            System.out.println("email enviado");
        } catch (EmailException e) {
            e.printStackTrace();
        }

    }

}
