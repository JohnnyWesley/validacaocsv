
import dao.sellDao;
import util.enviarEmail;

import javax.sound.midi.Soundbank;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanIn = null;
        String inputLine = "";
        enviarEmail mail = new enviarEmail();
        boolean isValid = true;
        File directoryPath = new File("PENDENTES");
        try {

            //coloca todos os arquivos em um array
            String[] files = directoryPath.list();

            for (int i = 0; i < files.length; i++) {
                if (files[i].endsWith(".csv")) {
                    Path source = Paths.get(directoryPath.getAbsolutePath() + "\\" + files[i]);
                    Path invalid = Paths.get(directoryPath.getAbsolutePath().replace("PENDENTES", "INVALIDADO") + "\\" + files[i]);
                    Path valid = Paths.get(directoryPath.getAbsolutePath().replace("PENDENTES", "VALIDADO") + "\\" + files[i]);
                    scanIn = new Scanner(new BufferedReader(new FileReader(String.valueOf(source))));
                    System.out.println("Iniciando leitura do arquivo " + files[i]);
                    //Se ele tiver uma linha, vai começar a ser lido.
                    //condição que verifica se o arquivo testá vazio
                    if(scanIn.hasNext()){
                        readLoop:
                        //enquanto o arquivo tiver linhas
                        while (scanIn.hasNextLine()) {
                            //aqui a linha do arquivo é atribuída a uma variável tipo String
                            inputLine = scanIn.nextLine();
                            //aqui roda a condição que de uma função boolean que retorna se essa linha que estamos lendo atualmente tem 4 colunas ou não
                            if (validateCols(inputLine)) {
                                // agora é feita a verificação se essa linha que foi posta na variável inLine está vazia, se ela estiver, não vai pra função da query
                                if (!inputLine.equals("")) {
                                    //função que põe essa linha em um array
                                    validateRows(inputLine);
                                }
                                //esse else é caso o arquivo possuir menos ou mais que 4 linhas.
                            } else {
                                System.out.println("Arquivo não atende o critério de possuir 4 colunas, logo, foi considerado inválido, será movido para o diretório /INVALIDADO");
                                isValid = false;
                                //aqui é feito um break no loop de leitura pra caso uma linha for falsa ela matar a execução do loop e não atribuir mais true na isValid;
                                break readLoop;
                            }
                            isValid = true;
                        }
                        // esse else é do if da verifição se o arquivo está vazio ou não.
                    }else{
                        isValid = false;
                        System.out.println("Arquivo vazio, será movido para o diretório /INVALIDADO");
                    }
                    //depois do while de leitura de cada arquivo essa condição vai verificar se no final esse arquivo é válido ou não.
                    if(isValid){
                        scanIn.close();
                        //o comando de enviar e-mail está comidato pois é necessário por o seu e-mail e senha para enviar, caso contrário, dará erro
                        //mail.enviar(String.valueOf(source), files[i]);
                        Files.move(source, valid);

                    }else{
                        scanIn.close();
                        //mover os arquivos se não forem válidos
                        Files.move(source, invalid);
                    }
                }
            }
        } catch (IOException e) {
            //System.out.println("Um dos diretórios: /PENDENTES, /INVALIDADO ou /VALIDADO não existem, verifique se os nomes dos mesmos estão corretos");
            e.printStackTrace();
        }
    }
    //essa função recebe a linha do inputLine como parâmetro e quebra ela em array que se divide a cada ";" ou seja, cada coluna de uma linha será posta em um índice de um array
    //podendo trabalhar cada string de forma isolada, podendo assim, mudar o padrão de data, por exemplo, e assim, põe todos esses índices em uma query que vai para o banco de dados
    private static void validateRows(String line){
        String query = "";
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date data = null;
        //função que quebra a string lida em um array
        String[] inArray = line.split(";");
        for(int i = 0; i < inArray.length; i++){
            //essa função aqui verifica se chegou no último índice do array, se estiver chegado ele executa a query
            if(i == inArray.length -1){
                data = new Date(inArray[2]);
                String strDate = formato.format(data);
                sellDao.insert("INSERT INTO sell(idSell, clientName, SellDate, price) VALUES(" + Integer.parseInt(inArray[0]) + ",'" + inArray[1] + "', '" + strDate + "'," + inArray[3].replace(",", ".") + ")");
            }
        }
    }
    //função que verifica o total de colunas em cada linha
    private static boolean validateCols(String line){
        boolean is4rows = true;
        /* Aqui primeiro é verificado se essa linha tem algum texto ou não, isso já é feito ali no código main acima, mas, eu escolhi validar se tem 4 colunas antes de rodar
         a condição que verifica se a linha é vazia no método MAIN, por conta que se o arquivo não atender esse critério ele não vai pra query, também a função ia ficar responsável por rodar a query
          e retornar o boolean se tem 4 colunas ou não, então ia ter que chamar ela duas vezes no método main, então preferi colocar uma função separada pra verificar colunas */
        if (!line.trim().equals("")){
            //System.out.println(line);
            //aqui põe a string separada a cada ";" em um array, cada vez que tiver um ; significa que eu tenho uma coluna no CSV, o método .split(";") quebra a string
            //em um índice do array toda vez que tiver o ; se o array tiver menos ou mais de 4 índices significa que ele não atendeu ao critério de 4 colunas.
            String[] inArrray = line.trim().split(";");
            //condição que verifica se o array tem menos ou mais que 4 colunas
            //System.out.println(inArrray.length);
            if(inArrray.length <4 || inArrray.length > 4){
                is4rows = false;
            }else{
                is4rows = true;
            }
        }
        return is4rows;
    }
}








